#!/bin/ksh

kubectl delete -f jenkins-master-service.yaml
kubectl delete -f jenkins-master.yaml
kubectl delete -f jenkins-slave.yaml

sleep 30

kubectl get pods 
kubectl get services

gcloud container --project "build-1004" clusters delete -q --zone "europe-west1-b" "cluster-1"
