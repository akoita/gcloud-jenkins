#!/bin/ksh

gcloud auth login

gcloud config set project build-1004

gcloud config set compute/zone europe-west1-b

#gcloud compute --project "build-1004" instance-groups managed create "cluster-1" --zone "europe-west1-b" --base-instance-name "cluster-1" --template "cluster-1-template" --size "4"

#gcloud container --project "build-1004" clusters create "cluster-1" --zone "europe-west1-b" --machine-type "n1-standard-1" --scope "https://www.googleapis.com/auth/compute","https://www.googleapis.com/auth/cloud.useraccounts","https://www.googleapis.com/auth/devstorage.read_write","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/cloud-platform" --network "default" --enable-cloud-logging --no-enable-cloud-monitoring

gcloud container --project "build-1004" clusters create "cluster-1" --num-nodes "2" --zone "europe-west1-b" --machine-type "n1-highcpu-4" --scope "https://www.googleapis.com/auth/compute","https://www.googleapis.com/auth/devstorage.read_write","https://www.googleapis.com/auth/logging.admin" --network "default" --enable-cloud-logging --no-enable-cloud-monitoring

gcloud config set container/cluster cluster-1

gcloud container clusters get-credentials cluster-1

kubectl create -f jenkins-master.yaml

sleep 60

kubectl get pods 

kubectl create -f jenkins-master-service.yaml

sleep 30

kubectl get services

kubectl create -f jenkins-slave.yaml

sleep 30

kubectl get pods 

echo "OK: Containers deployed ..."
